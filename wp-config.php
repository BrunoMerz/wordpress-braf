<?php
/**
 * As configurações básicas do WordPress
 *
 * O script de criação wp-config.php usa esse arquivo durante a instalação.
 * Você não precisa usar o site, você pode copiar este arquivo
 * para "wp-config.php" e preencher os valores.
 *
 * Este arquivo contém as seguintes configurações:
 *
 * * Configurações do MySQL
 * * Chaves secretas
 * * Prefixo do banco de dados
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Configurações do MySQL - Você pode pegar estas informações com o serviço de hospedagem ** //
/** O nome do banco de dados do WordPress */
define( 'DB_NAME', 'brafe' );

/** Usuário do banco de dados MySQL */
define( 'DB_USER', 'admin' );

/** Senha do banco de dados MySQL */
define( 'DB_PASSWORD', 'admin' );

/** Nome do host do MySQL */
define( 'DB_HOST', 'localhost' );

/** Charset do banco de dados a ser usado na criação das tabelas. */
define( 'DB_CHARSET', 'utf8mb4' );

/** O tipo de Collate do banco de dados. Não altere isso se tiver dúvidas. */
define( 'DB_COLLATE', '' );

/**#@+
 * Chaves únicas de autenticação e salts.
 *
 * Altere cada chave para um frase única!
 * Você pode gerá-las
 * usando o {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org
 * secret-key service}
 * Você pode alterá-las a qualquer momento para invalidar quaisquer
 * cookies existentes. Isto irá forçar todos os
 * usuários a fazerem login novamente.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'Na7*RFi~K8kB}a38W2*VT4n{0:@+$bI%>Isu0|_l+wwf4w`m0CyjbSJJ8hi>TCw$' );
define( 'SECURE_AUTH_KEY',  'AMCE:`I0Cd_4A2i=&wMNi.y#MiDh4)JLOY#cMK^M#,aoU,Ax:O?Y-:]x;`&*oCXD' );
define( 'LOGGED_IN_KEY',    ';m)k*Q*JK+6t~yEzQLkQ@!R6Y~}Y`!~CH/7j_EI}!b#pp_v6_D<qr}N;X37.F(e;' );
define( 'NONCE_KEY',        'FVYSI&/Yb.;O8!=f{n^sn8PY)z2I=xb.tp&gH*r?i^R*2`/:+fc0Pt[W/k)7=x|F' );
define( 'AUTH_SALT',        'f`Z(%c1/BHSnFDEySg2(4(Zw#:6r;3k:WgpL^%_9RL@Fd8l6V.x7FL:82AYwqy`3' );
define( 'SECURE_AUTH_SALT', 'SLJ+84J-$l/JpPL$)<Jv*4W9/n0;o!-k4G(0jW<jb8haO*rHbXB066|U6aap<]dz' );
define( 'LOGGED_IN_SALT',   ',L^ u8-{N:nJ7@m#VC2( slQ5H?|vs.IUj?nG!x#-r(&/rloep`&DcC6]Tf)c&wv' );
define( 'NONCE_SALT',       ',y@7WdDL;|sTKO=][Rr8q%TF.)Bj,lVz|Y^X+cBz/.&b-l_U9&fRhne4.+:(-4qJ' );

/**#@-*/

/**
 * Prefixo da tabela do banco de dados do WordPress.
 *
 * Você pode ter várias instalações em um único banco de dados se você der
 * um prefixo único para cada um. Somente números, letras e sublinhados!
 */
$table_prefix = 'wp_';

/**
 * Para desenvolvedores: Modo de debug do WordPress.
 *
 * Altere isto para true para ativar a exibição de avisos
 * durante o desenvolvimento. É altamente recomendável que os
 * desenvolvedores de plugins e temas usem o WP_DEBUG
 * em seus ambientes de desenvolvimento.
 *
 * Para informações sobre outras constantes que podem ser utilizadas
 * para depuração, visite o Codex.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Isto é tudo, pode parar de editar! :) */

/** Caminho absoluto para o diretório WordPress. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Configura as variáveis e arquivos do WordPress. */
require_once ABSPATH . 'wp-settings.php';
