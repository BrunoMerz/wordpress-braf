<head><title><?php the_field('page_title_produtos')?></title></head>
<div>
    <div class="block3">
        <div>
            <div><div class="circle1"><div class="circleborder1"><div class="circlecenter1"></div></div></div></div>
            <h1><?php the_field('circle_1_title') ?></h1>
            <p> <?php the_field('circle_1_txt') ?>
            </p>
        </div>
        <div>
            <div><div class="circle2"><div class="circleborder2"><div class="circlecenter2"></div></div></div></div>
            <h1><?php the_field('circle_2_title') ?></h1>
            <p><?php the_field('circle_2_txt') ?>
            </p>
        </div>
        <div>
            <div><div class="circle3"><div class="circleborder3"><div class="circlecenter3"></div></div></div></div>
            <h1>Mineiro</h1>
            <p>As condições climáticas não eram as melhores nessa primeira escolha e, entre 1800 e 1850, tentou-se o cultivo noutras regiões: o João Alberto Castelo Branco trouxe mudas do Pará
            </p>
        </div>

        <span class="saibamais">
            <div class="invert"><a href="<?php get_page_by_title(the_field('page_title_produtos')) ?>">Saiba Mais</a></div>
        </span>

    </div>
</div>