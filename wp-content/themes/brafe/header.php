<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <?php wp_head() ?>
</head>
<body>
    <div class = "HeaderDiv">
        <header class = "MainHeader">
            <div class = "IndexLogo"><a href="<?php if(is_front_page()){echo '#';}else{echo get_home_url();} ?>"><img src="<?php echo get_stylesheet_directory_uri() ?>/img/brafe2.png"></a></div>
            <?php
                $args = array(
                    'menu' => 'principal',
                    'theme_location' => 'navegacao',
                    'container' => false
                );
                wp_nav_menu( $args ) 
                ?>
        </header>
        <div class = "divisionbanner"></div>
    </div>