<?php 
    // Template Name: Home Page
?>


<?php get_header() ?>
<head><title><?php the_field('page_title_home')?></title></head>
    <div class="banner">
       <div>
           <div class="innerbox">
               <div><h1>
               <?php the_field('titulo_do_banner') ?>
               </h1></div>
                <div class ="line"></div>
                <div><p><?php the_field('subtitulo_do_banner') ?></p></div>
            </div>
       </div>
    </div>
    <div class="block2desc">
        <div>
            <div><h1><?php the_field('titulo_sobre') ?></h1></div>
            <div class="amorperfeição">
                <figure><img src="<?php the_field('foto_l') ?>"><div class="desctxt"><p><?php the_field('descricao_l') ?></p></div></figure>
                <figure><img src="<?php the_field('foto_r') ?>"><div class="desctxt"><p><?php the_field('descricao_r') ?></p></div></figure>
            </div>

            <div class="txt"><div><p><?php the_field('mini_texto') ?></p></div></div>
        </div>
        <span id="sobre"></span>
    </div>
    <div class = "division"></div>
    <span id="produtos"></span>
    
    <?php include get_template_directory().'/include/produtos.php'; ?>

    <div class = "division"></div>
    <span id="portfólio"></span>
    <div class="lastblock">
        <div class="item">
            <img src="<?php echo get_stylesheet_directory_uri() ?>/img/botafogo.jpg">
            <div class="itemtxt" id="itemtxt">
                <div><h1>Botafogo</h1></div>
                <div><p>As condições climáticas não eram as melhores nessa primeira escolha e, entre 1800 e 1850, tentou-se o cultivo</p></div>
                <div class="vermapabox"><a class="vermapa" href="https://www.google.com/maps/place/Origamid/@-22.9567307,-43.1920258,14.36z/data=!4m5!3m4!1s0x9be1553784b685:0x630552de6ab90fca!8m2!3d-22.9572162!4d-43.1761896">VER MAPA</a></div>
            </div>
        </div>
        <div class="item">
            <img src="<?php echo get_stylesheet_directory_uri() ?>/img/iguatemi.jpg">
            <div class="itemtxt" id="itemtxt">
                <div><h1>Iguatemi</h1></div>
                <div><p>As condições climáticas não eram as melhores nessa primeira escolha e, entre 1800 e 1850, tentou-se o cultivo</p></div>
                <div class="vermapabox"><a class="vermapa" href="">VER MAPA</a></div>
            </div>
        </div>
        <div class="item">
            <img src="<?php echo get_stylesheet_directory_uri() ?>/img/mineirao.jpg">
            <div class="itemtxt" id="itemtxt">
                <div><h1>Mineirão</h1></div>
                <div><p>As condições climáticas não eram as melhores nessa primeira escolha e, entre 1800 e 1850, tentou-se o cultivo</p></div>
                <div class="vermapabox"><a class="vermapa" href="">VER MAPA</a></div>
            </div>
        </div>
    </div>
    <span id="contato"></span>
    <footer class="news">
        <div class="newtxt"><h1>Assine Nossa Newsletter</h1><p>promoções e eventos mensais</p></div>
        <div class="newsinput"><input type="text" class="email" name="newsletteremail" placeholder=" Digite seu e-mail"><input class="emailsubmit" type="submit" value="Enviar"><div>
    </footer>
    <?php get_footer() ?>

